package com.example.hospitalreview.controller;

import com.example.hospitalreview.domain.dto.VisitCreateRequest;
import com.example.hospitalreview.domain.dto.VisitCreateResponse;
import com.example.hospitalreview.domain.dto.VisitResponse;
import com.example.hospitalreview.service.VisitService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/visits")
@Slf4j
@RequiredArgsConstructor
public class VisitController {

    private final VisitService visitService;

    // 방문 생성
    @PostMapping("")
    public ResponseEntity<VisitCreateResponse> createVisit(@RequestBody VisitCreateRequest dto, Authentication authentication) {
        String userName = authentication.getName();
        log.info("userName : {}", userName);
        log.info("방문 등록 성공");
        return ResponseEntity.ok().body(visitService.createVisit(dto, userName));

    }

    // 병원 방문 전체 조회
    @GetMapping("")
    public ResponseEntity<List<VisitResponse>> visitList(Pageable pageable) {
        log.info("병원 방문 전체 조회 성공");
        return ResponseEntity.ok().body(visitService.findAllVisit(pageable));
    }

    // 특정 유저의 방문 기록 조회
    @GetMapping("/users/{userId}")
    public ResponseEntity<List<VisitResponse>> findUserVisits(@PathVariable("userId") Long userId) {
        log.info("특정 유저의 방문 기록 조회 성공");
        return ResponseEntity.ok().body(visitService.findByUserId(userId));
    }

    // 특정 병원의 방문 기록 조회
    @GetMapping("/hospitals/{hospitalId}")
    public ResponseEntity<List<VisitResponse>> findHospitalVisits(@PathVariable("hospitalId") Long hospitalId) {
        log.info("특정 병원의 방문 기록 조회 성공");
        log.info("crontab 적용 확인을 위한 로그");
        return ResponseEntity.ok().body(visitService.findByHospitalId(hospitalId));
    }

}
