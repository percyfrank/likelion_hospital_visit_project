package com.example.hospitalreview.domain.dto;

import com.example.hospitalreview.domain.entity.Visit;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
@Builder
public class VisitResponse {

    private String hospitalName;
    private String userName;
    private String disease;
    private float amount;
    private LocalDateTime createdAt;

    public static VisitResponse of(Visit visit) {
        return VisitResponse.builder()
                .hospitalName(visit.getHospital().getHospitalName())
                .userName(visit.getUser().getUserName())
                .disease(visit.getDisease())
                .amount(visit.getAmount())
                .createdAt(visit.getCreatedAt())
                .build();
    }
}
