package com.example.hospitalreview.domain.dto;

import com.example.hospitalreview.domain.entity.User;
import com.example.hospitalreview.domain.entity.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
public class UserJoinRequest {  // 회원가입 요청
    private String userName;
    private String password;
    private String email;
    private UserRole role;

    public User toEntity(String password) {
        return User.builder()
                .userName(this.userName)
                .password(password)     // 암호화된 password
                .emailAddress(this.email)
                .role(this.role)
                .build();
    }
}