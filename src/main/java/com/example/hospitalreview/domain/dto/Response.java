package com.example.hospitalreview.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Response<T> {  // 모든 api 결과를 Response 객체로 감싸서 리턴, 결과를 통일성있게 작성 가능

    private String resultCode;      // 에러나 성공 서버 코드
    private T result;

    public static Response<Void> error(String resultCode) {
        return new Response(resultCode, null);
    }

    public static <T> Response<T> success(T result) {
        return new Response("SUCCESS", result);
    }

}