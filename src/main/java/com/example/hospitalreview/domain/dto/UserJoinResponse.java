package com.example.hospitalreview.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class UserJoinResponse { // 회원가입 응답
    private String userName;
    private String email;
}
