package com.example.hospitalreview.service;

import com.example.hospitalreview.configuration.JwtTokenUtil;
import com.example.hospitalreview.domain.dto.UserDto;
import com.example.hospitalreview.domain.dto.UserJoinRequest;
import com.example.hospitalreview.domain.entity.User;
import com.example.hospitalreview.exception.ErrorCode;
import com.example.hospitalreview.exception.HospitalReviewException;
import com.example.hospitalreview.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;

    @Value("${jwt.token.secret}")   //spring 제공 라이브러리 application.yml 접근
    private String secretKey;
    private long expireTimeMs = 1000 * 60 * 60;     // 1시간 유효

    public UserDto join(UserJoinRequest request) {

        // 회원 가입
        // 회원 userName(id) 중복 check
        // 중복이면 예외 발생
        userRepository.findByUserName(request.getUserName())
                .ifPresent((user) -> {
                    throw new HospitalReviewException(ErrorCode.DUPLICATED_USER_NAME,
                            String.format("UserName : %s",request.getUserName()));
                });

        // 회원가입 save
        User savedUser = userRepository.save(request.toEntity(encoder.encode(request.getPassword())));

        return UserDto.builder()
                .id(savedUser.getId())
                .userName(savedUser.getUserName())
                .email(savedUser.getEmailAddress())
                .build();
    }

    public String login(String userName, String password) {

        // userName 있는지 확인
        // 없으면 NOT_FOUND 발생
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new HospitalReviewException(ErrorCode.NOT_FOUND,
                        String.format("%s는 가입한 적이 없습니다.", userName)));

        // password 일치 하는지 여부 확인
        if(!encoder.matches(password, user.getPassword())){
            throw new HospitalReviewException(ErrorCode.INVALID_PASSWORD,
                    String.format("password에서 오류 발생합니다."));
        }

        // 두 가지 확인중 예외 안났으면 Token발행
        return JwtTokenUtil.createToken(userName, secretKey, expireTimeMs);

    }


    public User getUserByUserName(String userName) {
        return userRepository.findByUserName(userName)
                .orElseThrow(() -> new HospitalReviewException(ErrorCode.NOT_FOUND, ""));
    }
}
