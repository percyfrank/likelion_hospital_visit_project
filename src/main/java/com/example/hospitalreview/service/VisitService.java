package com.example.hospitalreview.service;

import com.example.hospitalreview.domain.dto.VisitCreateRequest;
import com.example.hospitalreview.domain.dto.VisitCreateResponse;
import com.example.hospitalreview.domain.dto.VisitResponse;
import com.example.hospitalreview.domain.entity.Hospital;
import com.example.hospitalreview.domain.entity.User;
import com.example.hospitalreview.domain.entity.Visit;
import com.example.hospitalreview.exception.ErrorCode;
import com.example.hospitalreview.exception.HospitalReviewException;
import com.example.hospitalreview.repository.HospitalRepository;
import com.example.hospitalreview.repository.UserRepository;
import com.example.hospitalreview.repository.VisitRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class VisitService {

    private final UserRepository userRepository;
    private final HospitalRepository hospitalRepository;
    private final VisitRepository visitRepository;
    public VisitCreateResponse createVisit(VisitCreateRequest dto, String userName) {

        // request dto로 넘어온 hospitalId로 찾고, 없으면 등록 불가
        Hospital hospital = hospitalRepository.findById(dto.getHospitalId())
                .orElseThrow(() -> new HospitalReviewException(ErrorCode.NOT_FOUND, "해당 병원 id가 없습니다."));

        // Authentication.getName()으로 넘어온 userName으로 찾고, 없으면 등록 불가
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new HospitalReviewException(ErrorCode.NOT_FOUND, "해당 유저가 존재하지 않습니다"));

        Visit visit = Visit.builder()
                .user(user)
                .hospital(hospital)
                .disease(dto.getDisease())
                .amount(dto.getAmount())
                .build();

        Visit savedVisit = visitRepository.save(visit);

        return VisitCreateResponse.of(savedVisit);

    }

    public List<VisitResponse> findAllVisit(Pageable pageable) {
        Page<Visit> visits = visitRepository.findAll(pageable);
        return visits.stream().map(VisitResponse::of).collect(Collectors.toList());
    }

    public List<VisitResponse> findByUserId(Long userId) {
        List<Visit> findByUserVisit = visitRepository.findByUserId(userId);
        return findByUserVisit.stream().map(VisitResponse::of).collect(Collectors.toList());
    }


    public List<VisitResponse> findByHospitalId(Long hospitalId) {
        List<Visit> findHospitalVisit = visitRepository.findByHospitalId(hospitalId);
        return findHospitalVisit.stream().map(VisitResponse::of).collect(Collectors.toList());

    }
}