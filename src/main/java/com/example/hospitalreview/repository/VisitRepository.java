package com.example.hospitalreview.repository;

import com.example.hospitalreview.domain.entity.Visit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VisitRepository extends JpaRepository<Visit,Long> {

    List<Visit> findByUserId(Long userId);
    List<Visit> findByHospitalId(Long hospitalId);
}
