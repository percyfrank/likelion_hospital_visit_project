package com.example.hospitalreview.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@Getter
public enum ErrorCode {

    DUPLICATED_USER_NAME(HttpStatus.CONFLICT, "User name이 중복됩니다."),
    NOT_FOUND(HttpStatus.NOT_FOUND,"UserName을 찾을 수 없습니다."),
    INVALID_PASSWORD(HttpStatus.BAD_REQUEST, "패스워드가 틀렸습니다.");

    private HttpStatus status;
    private String message;
}
