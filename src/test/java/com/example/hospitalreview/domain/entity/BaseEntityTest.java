package com.example.hospitalreview.domain.entity;

import com.example.hospitalreview.repository.VisitRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class BaseEntityTest {

    @Autowired
    VisitRepository visitRepository;

    @Test
    public void auditingTest() throws Exception {
        //given
        Visit visit = new Visit();
//        Hospital hospital = new Hospital();
//        User user = new User();
        visit.setId(1L);
//        visit.setHospital(new Hospital(1L, "A치과", "서울시 노원구"));
        visit.setDisease("통증");
        visit.setAmount(5000);

        //when
        Visit savedVisit = visitRepository.save(visit);

        //then
        System.out.println("savedVisit.getCreatedAt() = " + savedVisit.getCreatedAt());

    }

}